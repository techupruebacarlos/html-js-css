function getClientesGermany() {
  var url = "http://services.odata.org/V4/Northwind/Northwind.svc/Customers?$filter=Country eq 'Germany'";
  var request = new XMLHttpRequest();
  request.onreadystatechange = function () {
    if (this.readyState==4 && this.status == 200){
      console.log(request.responseText);
    }
  }
  request.open("GET",url,true);
  request.send();
}

var clientesObtenidos;
var imagenPais;

function getClientes() {
  var url = "http://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();
  request.onreadystatechange = function () {
    if (this.readyState==4 && this.status == 200){
      console.log(request.responseText);
      clientesObtenidos = request.responseText;
      procesarClientes();
    }
  }
  request.open("GET",url,true);
  request.send();
}

function procesarClientes() {
  var JSONProductos= JSON.parse(clientesObtenidos);
  var tabla = document.getElementById('tablaClientes');
  for (var i = 0; i < JSONProductos.value.length; i++) {
    var nuevaFila = document.createElement("tr");
    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = JSONProductos.value[i].ContactName;
    var columnaCompania = document.createElement("td");
    columnaCompania.innerText = JSONProductos.value[i].CompanyName;
    var columnaPhone = document.createElement("td");
    columnaPhone.innerText = JSONProductos.value[i].Phone;
    var columnaCity = document.createElement("td");
    columnaCity.innerText = JSONProductos.value[i].City;
    var columnaBandera = document.createElement("td");
    imagenPais = "https://www.countries-ofthe-world.com/flags-normal/flag-of-"+JSONProductos.value[i].Country+".png";
    var imgCountry = document.createElement("img");
    imgCountry.src=imagenPais;
    //imgCountry.style="width:30px";
    imgCountry.classList.add("flag");
    columnaBandera.appendChild(imgCountry);
    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaCompania);
    nuevaFila.appendChild(columnaPhone);
    nuevaFila.appendChild(columnaCity);
    nuevaFila.appendChild(columnaBandera);

    tabla.appendChild(nuevaFila);

    console.log(JSONProductos.value[i].ProductName);
    //alert(JSONProductos.value[i].ProductName);
  }
}
